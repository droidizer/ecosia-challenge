package com.guru.ecosiachallenge;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.subjects.BehaviorSubject;

public class MediaFileManager implements IMediaFileManager {
    private Disposable disposable = Disposables.disposed();
    private BehaviorSubject<List<Long>> behaviorSubject = BehaviorSubject.create();

    @Override
    public Observable<List<Long>> getMediaFilesOnDevice(Context context) {
        Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

        disposable = Observable.just(context)
                .subscribe(res ->
                        {
                            if (behaviorSubject.hasValue()) {
                                behaviorSubject.getValue();
                            } else {
                                try (Cursor cursor = context.getContentResolver().query(uri, null, null, null, null, null)) {

                                    if (cursor != null) {
                                        List<Long> mp3Files = new ArrayList<>();
                                        if (cursor.moveToFirst()) {
                                            int id = cursor.getColumnIndex(MediaStore.Audio.Media._ID);
                                            do {
                                                long thisId = cursor.getLong(id);
                                                mp3Files.add(thisId);
                                            } while (cursor.moveToNext());
                                        }

                                        behaviorSubject.onNext(mp3Files);
                                    }
                                } catch (Exception e) {
                                    behaviorSubject.onError(new Exception("Something Went Wrong!"));
                                }
                            }
                        }, throwable -> behaviorSubject.onError(throwable)
                );

        return behaviorSubject.hide();
    }

    void clear() {
        if (!disposable.isDisposed()) {
            disposable.dispose();
        }
    }
}
