package com.guru.ecosiachallenge;

import android.Manifest;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;

public class PlayBackgroundAudioActivity extends AppCompatActivity {

    private static final int MY_PERMISSION_REQUEST_CODE = 123;

    private MediaFileManager mediaFileManager = new MediaFileManager();

    private MusicServiceBinder audioServiceBinder = null;

    private Disposable progressDisposable = Disposables.disposed();

    private Disposable disposable = Disposables.disposed();

    private SeekBar backgroundAudioProgress;

    private TextView audioFileUriTextView;

    private Button startBackgroundAudio;

    private Button pauseBackgroundAudio;

    private Button stopBackgroundAudio;

    private boolean isPermissionGranted;

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            audioServiceBinder = (MusicServiceBinder) iBinder;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            audioServiceBinder.clear();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(R.string.play_music);
        scanMedia();
        bindAudioService();
        checkPermission();

        backgroundAudioProgress = findViewById(R.id.service_progressbar);
        audioFileUriTextView = findViewById(R.id.result);
        startBackgroundAudio = findViewById(R.id.play);
        pauseBackgroundAudio = findViewById(R.id.pause);
        stopBackgroundAudio = findViewById(R.id.stop);

        startBackgroundAudio.setOnClickListener(view -> {
            backgroundAudioProgress.setVisibility(View.VISIBLE);
            playMedia();
        });

        stopBackgroundAudio.setOnClickListener(view -> {
            audioServiceBinder.stopAudio();
            backgroundAudioProgress.setProgress(0);
            Toast.makeText(getApplicationContext(), "Stopped", Toast.LENGTH_SHORT).show();
        });

        pauseBackgroundAudio.setOnClickListener(view -> {
            audioServiceBinder.pauseAudio();
            Toast.makeText(getApplicationContext(), "Paused", Toast.LENGTH_SHORT).show();
        });
    }

    private void scanMedia() {
        Intent mediaScanIntent = new Intent(
                Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.parse(Environment.getExternalStorageDirectory().getAbsolutePath());
        mediaScanIntent.setData(contentUri);
        PlayBackgroundAudioActivity.this.sendBroadcast(mediaScanIntent);
    }

    protected void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    ActivityCompat.requestPermissions(PlayBackgroundAudioActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            MY_PERMISSION_REQUEST_CODE
                    );
                } else {
                    ActivityCompat.requestPermissions(
                            this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            MY_PERMISSION_REQUEST_CODE
                    );
                }
            } else {
                isPermissionGranted = true;
            }
        }
    }

    private void hideButtons() {
        startBackgroundAudio.setVisibility(View.GONE);
        pauseBackgroundAudio.setVisibility(View.GONE);
        stopBackgroundAudio.setVisibility(View.GONE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isPermissionGranted = true;
                } else {
                    audioFileUriTextView.setText(R.string.permission_required);
                    hideButtons();
                }
            }
        }
    }

    private void playMedia() {
        if (!audioServiceBinder.isPlaying()) {
            updateProgress();

            disposable = mediaFileManager.getMediaFilesOnDevice(this)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(mediaFileList -> {
                                if (!mediaFileList.isEmpty()) {
                                    Collections.shuffle(mediaFileList);
                                    Uri uri = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, mediaFileList.get(0));
                                    audioServiceBinder.setAudioFileUri(uri);
                                    audioServiceBinder.setContext(getApplicationContext());
                                    audioServiceBinder.startAudio();
                                    Toast.makeText(getApplicationContext(), "Playing", Toast.LENGTH_SHORT).show();
                                } else {
                                    backgroundAudioProgress.setVisibility(View.GONE);
                                    Toast.makeText(getApplicationContext(), "Media Not Found!", Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show());
        }
    }

    private void updateProgress() {
        progressDisposable = audioServiceBinder.getAudioProgress()
                .filter(res -> res != 0)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(currProgress -> {
                            audioFileUriTextView.setText(audioServiceBinder.getAudioFileUri().toString());
                            backgroundAudioProgress.setProgress(currProgress);
                        }
                        , throwable -> {
                            backgroundAudioProgress.setProgress(0);
                            Toast.makeText(getApplicationContext(), "Unable to update progress.", Toast.LENGTH_SHORT).show();
                        }
                );
    }

    private void bindAudioService() {
        if (audioServiceBinder == null) {
            Intent intent = new Intent(PlayBackgroundAudioActivity.this, MusicPlayerService.class);
            bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        }
    }

    private void unBoundAudioService() {
        if (audioServiceBinder != null) {
            unbindService(serviceConnection);
        }
    }

    @Override
    protected void onDestroy() {
        audioServiceBinder.clear();
        mediaFileManager.clear();
        unBoundAudioService();
        
        if (!disposable.isDisposed()) {
            disposable.dispose();
        }
        if (!progressDisposable.isDisposed()) {
            progressDisposable.dispose();
        }
        super.onDestroy();
    }
}