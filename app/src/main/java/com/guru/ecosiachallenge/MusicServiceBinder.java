package com.guru.ecosiachallenge;


import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

class MusicServiceBinder extends Binder {

    private Uri audioFileUri = null;

    private MediaPlayer audioPlayer = null;

    private Context context = null;

    private boolean isPaused;

    void setContext(Context context) {
        this.context = context;
    }

    Uri getAudioFileUri() {
        return audioFileUri;
    }

    void setAudioFileUri(Uri audioFileUri) {
        this.audioFileUri = audioFileUri;
    }

    void startAudio() {
        initAudioPlayer();
        playAudio();
    }

    void pauseAudio() {
        if (audioPlayer != null) {
            audioPlayer.pause();
            isPaused = true;
        }
    }

    void stopAudio() {
        if (audioPlayer != null) {
            audioPlayer.stop();
            destroyAudioPlayer();
            isPaused = false;
        }
    }

    Observable<Integer> getAudioProgress() {
        int ret = 0;

        return Observable.interval(1000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .map(res -> {
                    int currAudioPosition = getCurrentAudioPosition();
                    int totalAudioDuration = getTotalAudioDuration();
                    if (totalAudioDuration > 0) {
                        return ((currAudioPosition * 100) / totalAudioDuration);
                    }

                    return ret;
                });
    }

    boolean isPlaying() {
        if (audioPlayer != null) {
            return audioPlayer.isPlaying();
        }

        return false;
    }

    void clear() {
        if (audioPlayer != null) {
            audioPlayer.reset();
            audioPlayer.release();
            audioPlayer = null;
        }
    }

    private Context getContext() {
        return context;
    }

    private void initAudioPlayer() {
        if (audioPlayer == null) {
            audioPlayer = new MediaPlayer();
        }
    }

    private void playAudio() {
        if (!audioPlayer.isPlaying() && isPaused) {
            audioPlayer.start();
        } else {
            if (getAudioFileUri() != null) {
                try {
                    audioPlayer.setDataSource(getContext(), getAudioFileUri());
                    audioPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                audioPlayer.start();
                isPaused = false;
            }
        }
    }

    private void destroyAudioPlayer() {
        if (audioPlayer != null) {
            if (audioPlayer.isPlaying()) {
                audioPlayer.stop();
            }

            audioPlayer.release();
            audioPlayer = null;
        }
    }

    private int getCurrentAudioPosition() {
        if (audioPlayer != null) {
            return audioPlayer.getCurrentPosition();
        }
        return 0;
    }

    private int getTotalAudioDuration() {
        if (audioPlayer != null) {
            return audioPlayer.getDuration();
        }
        return 0;
    }
}
