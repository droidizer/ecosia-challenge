package com.guru.ecosiachallenge;

import android.content.Context;

import java.util.List;

import io.reactivex.Observable;

interface IMediaFileManager {

    Observable<List<Long>> getMediaFilesOnDevice(Context context);

}
