This repository demonstrates a Sample Music Player written in Java.

App features include:

1. Check Read External Disc Permissions. 
2. Media playback: Play, Pause and Stop events.
3. Background playback of music. 
4. Handled errors when 
- no media found on device
- no permission is provided
5. Scanning device for mounted media to have latest media updates like addition of Mp3 files. 

Todo: 

1. Refactoring of code to extract hardcoded strings and other to fit MVVM architecture.
2. Need to update UI changes after returning from background. 
3. Fixing of some cosmetic/UI changes. 
4. Add tests. 

Bug: 

1. After playing completes new song can be played(Start New Song) only after clicking on Stop. 
2. Laggy progress Update and Playlist display needs to be fixed . 
